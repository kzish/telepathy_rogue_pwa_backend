﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using company.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace company.Controllers
{
    [Authorize]
    [Route("Settings")]
    public class SettingsController : Controller
    {
        //IHostingEnvironment host;
        telepathyContext db = new telepathyContext();
        private string source = "admin.Controllers.SettingsController.";

        public SettingsController(IHostingEnvironment host)
        {
           // this.host = host;
        }

        [HttpGet("Logos")]
        public IActionResult Logos()
        {
            ViewBag.title = "Logos";
            var logos = db.MScannableLogos.Where(i => i.Company == User.Identity.Name).ToList();
            ViewBag.logos = logos;
            return View();
        }

        [HttpGet("ArVideo")]
        public IActionResult ArVideo()
        {
            ViewBag.title = "ArVideo";
            var video = new MArVideo();
            var videos = db.MArVideo.Where(i => i.Company == User.Identity.Name).ToList();
            if (videos.Count > 0)
            {
                video = videos.First();
            }
            ViewBag.video = video;
            return View();
        }

        //upload the logos
        [HttpPost("Logos")]
        public async Task<IActionResult> Logos(List<IFormFile> files)
        {
            try
            {
                ViewBag.title = "Logos";
                //save all the files
                List<MScannableLogos> logos = new List<MScannableLogos>();
                foreach (var file in files)
                {
                    var filename = $"{Guid.NewGuid().ToString()}.{Path.GetExtension(file.FileName)}";
                    var filepath = $"{Globals.uploads_folder}/{filename}";
                    using (var stream = System.IO.File.Create(filepath))
                    {
                        await file.CopyToAsync(stream);
                        stream.Dispose();
                    }
                    var logo = new MScannableLogos();
                    logo.ScannableLogo = $"{filename}";
                    logo.Company = User.Identity.Name;
                    logos.Add(logo);
                }
                db.MScannableLogos.AddRange(logos);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file(source + "Logos[post]", ex.Message);
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("Logos");
        }

        //upload the ar video
        [HttpPost("ArVideo")]
        public async Task<IActionResult> ArVideo(IFormFile file)
        {
            try
            {
                ViewBag.title = "ArVideo";
                //first delete the existing video
                var existing_video = db.MArVideo.Where(i => i.Company == User.Identity.Name).FirstOrDefault();
                if (existing_video != null)
                {
                    System.IO.File.Delete(Globals.uploads_folder + "/" + existing_video.ArVideo);
                    db.MArVideo.Remove(existing_video);
                    db.SaveChanges();
                }

                //save the video
                var filename = $"{Guid.NewGuid().ToString()}.{Path.GetExtension(file.FileName)}";
                var filepath = $"{Globals.uploads_folder}/{filename}";
                using (var stream = System.IO.File.Create(filepath))
                {
                    await file.CopyToAsync(stream);
                    stream.Dispose();
                }
                var video = new MArVideo();
                video.Company = User.Identity.Name;
                video.ArVideo = $"{filename}";
                db.MArVideo.Add(video);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file(source + "ArVideo[post]", ex.Message);
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("ArVideo");
        }



        [HttpGet("DeleteLogo/{id}")]
        public IActionResult DeleteLogo(int id)
        {
            try
            {
                var logo = db.MScannableLogos.Find(id);
                if (logo.ScannableLogo != null)
                {
                    System.IO.File.Delete($"{Globals.uploads_folder}/{logo.ScannableLogo}");
                }
                db.Remove(logo);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Logo removed";
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file(source + "DeleteLogo", ex.Message);
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("Logos");
        }

        [HttpGet("DeleteARVideo/{id}")]
        public IActionResult DeleteARVideo(int id)
        {
            try
            {
                var video = db.MArVideo.Find(id);
                if (video.ArVideo != null)
                {
                    System.IO.File.Delete($"{Globals.uploads_folder}/{video.ArVideo}");
                }
                db.Remove(video);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "VIdeo removed";
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file(source + "DeleteARVideo", ex.Message);
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("ArVideo");
        }

        [HttpGet("DeleteCompanyLogo/{id}")]
        public IActionResult DeleteCompanyLogo(string id) {
            try
            {
                var company = db.MCompany.Where(i => i.Email == User.Identity.Name).FirstOrDefault();
                if (!string.IsNullOrEmpty(company.CompanyIcon))
                {
                    System.IO.File.Delete(Globals.uploads_folder + "/" + company.CompanyIcon);
                }
                company.CompanyIcon = null;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Logo Deleted";
            } catch (Exception ex)
            {
                Globals.log_data_to_file(source + "DeleteCompanyLogo", ex.Message);
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("CompanyLogo");
        }


        //show page
        [HttpGet("CompanyLogo")]
        public IActionResult CompanyLogo()
        {
            ViewBag.title = "CompanyLogo";
            ViewBag.company = db.MCompany.Where(i => i.Email == User.Identity.Name).FirstOrDefault();
            return View();
        }

        //upoad company logo
        [HttpPost("CompanyLogo")]
        public async Task<IActionResult> CompanyLogo(IFormFile file)
        {
            try
            {
                ViewBag.title = "CompanyLogo";
                //first delete the existing icon
                var existing_company = db.MCompany.Where(i => i.Email == User.Identity.Name).FirstOrDefault();
                if (!string.IsNullOrEmpty(existing_company.CompanyIcon))
                {
                    System.IO.File.Delete(Globals.uploads_folder + "/" + existing_company.CompanyIcon);
                    existing_company.CompanyIcon = null;
                    db.SaveChanges();
                }
                //save the icon
                var filename = $"{Guid.NewGuid().ToString()}.{Path.GetExtension(file.FileName)}";
                var filepath = $"{Globals.uploads_folder}/{filename}";
                using (var stream = System.IO.File.Create(filepath))
                {
                    await file.CopyToAsync(stream);
                    stream.Dispose();
                }
                existing_company.CompanyIcon = $"{filename}";
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file(source + "CompanyLogo[post]", ex.Message);
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("CompanyLogo");
        }

        /// <summary>
        /// return the picture from the file path
        /// </summary>
        /// <param name="image_path"></param>
        /// <returns></returns>
        [HttpGet("GetPicture")]
        public async Task<IActionResult> GetPicture(string image_path)
        {
            var filePath = System.IO.Path.Combine(Globals.uploads_folder, image_path);
            var memory = new MemoryStream();
            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
                stream.Dispose();
            }
            memory.Position = 0;
            return File(memory.ToArray(), "image/jpg");
        }
        
        [HttpGet("GetVideo")]
        public async Task<IActionResult> GetVideo(string video_path)
        {
            var filePath = System.IO.Path.Combine(Globals.uploads_folder, video_path);
            var memory = new MemoryStream();
            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
                stream.Dispose();
            }
            memory.Position = 0;
            return File(memory.ToArray(), "video/mp4");
        }

        [HttpGet("CompanyName")]
        public IActionResult CompanyName() {
            ViewBag.title = "CompanyName";
            ViewBag.company = db.MCompany.Where(i => i.Email == User.Identity.Name).FirstOrDefault();
            return View();
        }

        [HttpPost("CompanyName")]
        public IActionResult CompanyName(string Name,string CompanySlogan) {
            try
            {
                var company = db.MCompany.Where(i => i.Email == User.Identity.Name).FirstOrDefault();
                company.Name = Name;
                company.CompanySlogan = CompanySlogan;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
            }
            catch (Exception ex) {
                Globals.log_data_to_file(source + "CompanyName[post]", ex.Message);
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("CompanyName");
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }
    }
}
