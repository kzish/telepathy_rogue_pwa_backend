﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using company.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Authorization;

namespace admin.Controllers
{
    [Route("Posts")]
    [Authorize]
    public class PostsController : Controller
    {
        private string source = "admin.Controllers.PostsController.";

        UserManager<IdentityUser> userManager;
        SignInManager<IdentityUser> SignInManager;
        //IHostingEnvironment host;
        telepathyContext db = new telepathyContext();

        public PostsController(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager,IHostingEnvironment host)
        {
            this.userManager = userManager;
            this.SignInManager = signInManager;
            //this.host = host;
        }


       
        //sho all companies
        [HttpGet("All")]
        public IActionResult All()
        {
            //delete empty posts
            var empty_posts = db.MPosts.Where(i => i.Title == null).ToList();
            db.RemoveRange(empty_posts);
            db.SaveChanges();

            ViewBag.title = "All Posts";
            var posts = db.MPosts.Where(i=>i.Company == User.Identity.Name).ToList().OrderByDescending(i => i.Date).ToList();
            ViewBag.posts = posts;
            return View();
        }

        //automatically creates a post and sends back the post to the main page
        [HttpGet("CreateNewPost/{id}")]
        public IActionResult CreateNewPost(int id)
        {
            ViewBag.title = "Create New Post";
            MPosts post = null;
            if (id==0)
            {
                post = new MPosts();
                post.Date = DateTime.Now;
                post.Company = User.Identity.Name;
                db.MPosts.Add(post);
                db.SaveChanges();
            }
            else
            {
                post = db.MPosts.Find(id);
                if (post == null) //incase there is no post redirect
                { 
                    return RedirectToAction("CreateNewPost", new { id = 0 });
                }
            }
            ViewBag.post = post;
            return View();
        }


        /// <summary>
        /// this method will update an existing post becauese the post exists already
        /// </summary>
        /// <param name="Post"></param>
        /// <returns></returns>
        [HttpPost("CreateNewPost")]
        public async Task<IActionResult> CreateNewPost(MPosts Post, IFormFile cover_image)
        {
            try
            {
                //save file
                //delete former pic and upload a new one
                var  old_post= db.MPosts.Find(Post.Id);
                if (old_post.CoverImage != null)
                {
                    System.IO.File.Delete($"{Globals.uploads_folder}/{old_post.CoverImage}");
                }
                if (cover_image != null)
                {
                    var filename = $"{Guid.NewGuid().ToString()}.{Path.GetExtension(cover_image.FileName)}";
                    var filepath = $"{Globals.uploads_folder}/{filename}";
                    using (var stream = System.IO.File.Create(filepath))
                    {
                        await cover_image.CopyToAsync(stream);
                        stream.Dispose();
                    }
                    old_post.CoverImage = $"{filename}";
                }
                //db.Entry(Post).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                old_post.Content = Post.Content;
                old_post.Title = Post.Title;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Post saved";
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file(source + "CreateNewPost", ex.Message);
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("All","Posts");
        }

        [HttpGet("DeletePost/{id}")]
        public IActionResult DeletePost(int id)
        {
            try
            {
                var post = db.MPosts.Where(i => i.Id == id).FirstOrDefault();

                //remove image
                if (post.CoverImage != null)
                {
                    System.IO.File.Delete($"{Globals.uploads_folder}/{post.CoverImage}");
                }
                db.Remove(post);
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Post deleted.";
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file(source + "DeletePosts", ex);
                TempData["type"] = "error";
                TempData["msg"] = "Error Occured";
            }
            return RedirectToAction("All", "Posts");
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }
    }
}
