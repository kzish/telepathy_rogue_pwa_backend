﻿using System;
using System.Collections.Generic;

namespace company.Models
{
    public partial class MArScannable
    {
        public int Id { get; set; }
        public string ScannableLogo { get; set; }
        public int? ArOverlayMimeType { get; set; }
        public string ArOverlayContent { get; set; }
        public string Company { get; set; }
    }
}
