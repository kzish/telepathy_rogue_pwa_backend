﻿using System;
using System.Collections.Generic;

namespace admin.Models
{
    public partial class MArVideo
    {
        public int Id { get; set; }
        public string ArVideo { get; set; }
        public string Company { get; set; }
    }
}
