﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using admin.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using admin.Models;

namespace admin.Controllers
{
    [Authorize]
    [Route("Home")]
    [Route("")]
    public class HomeController : Controller
    {

        telepathyContext db = new telepathyContext();
        [Route("Dashboard")]
        [Route("")]
        public IActionResult Dashboard()
        {
            ViewBag.title = "Dashboard";
            return View();
        }

       

       

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

    }
}
