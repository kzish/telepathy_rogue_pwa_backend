﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using admin.Models;
using Microsoft.AspNetCore.Identity;
using System.IO;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace admin.Controllers
{
    [Route("Company")]
    public class CompanyController : Controller
    {
        private string source = "admin.Controllers.CompanyController.";

        UserManager<IdentityUser> userManager;
        SignInManager<IdentityUser> SignInManager;

        public CompanyController(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager)
        {
            this.userManager = userManager;
            this.SignInManager = signInManager;
        }
        telepathyContext db = new telepathyContext();
        [HttpGet("CreateNewCompany")]
        public IActionResult CreateNewCompany()
        {
            ViewBag.title = "Create New Company";
            return View();
        }

        //sho all companies
        [HttpGet("All")]
        public IActionResult All()
        {
            ViewBag.title = "All companies";
            var companys = db.MCompany.ToList();
            ViewBag.companys = companys;
            return View();
        }

        [HttpGet("EditCompany/{id}")]
        public IActionResult EditCompany(int id)
        {
            ViewBag.title = "Edit Company";
            var company = db.MCompany.Find(id);
            ViewBag.company = company;
            return View();
        }

        [HttpPost("EditCompany")]
        public IActionResult EditCompany(MCompany company)
        {
            try
            {
                db.Entry(company).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                db.SaveChanges();
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
            }
            catch (Exception ex)
            {
                TempData["type"] = "success";
                TempData["msg"] = "Saved";
                Globals.log_data_to_file(source + "EditCompany", ex.Message);
                return RedirectToAction($"EditCompany/{company.Id}");
            }
            return RedirectToAction("All");
        }

        [HttpPost("CreateNewCompany")]
        public async Task<IActionResult> CreateNewCompany(MCompany company, string password)
        {
            try
            {
                ViewBag.title = "Create New Company";
                //create company and register it
                var exist = db.Aspnetusertokens.Where(i => i.Name == company.Email).FirstOrDefault();
                if (exist != null)
                {
                    TempData["type"] = "error";
                    TempData["msg"] = "This email is taken";
                    return View();
                }
                var user = new IdentityUser()
                {
                    Email = company.Email,
                    UserName = company.Email,
                    PasswordHash = password
                };
                var result = await userManager.CreateAsync(user, password);
                if (result.Succeeded)
                {
                    db.MCompany.Add(company);
                    db.SaveChanges();
                    TempData["type"] = "success";
                    TempData["msg"] = "User Account created";
                }
                else
                {
                    var errors = string.Empty;
                    foreach(var e in result.Errors)
                    {
                        errors += e.Description + " ";
                    }
                    TempData["type"] = "error";
                    TempData["msg"] = errors;
                }
            }catch(Exception ex)
            {
                Globals.log_data_to_file(source + "CreateNewCompany",ex.Message);
                TempData["type"] = "error";
                TempData["msg"] = ex.Message;
            }
            return RedirectToAction("All");
        }

        [HttpGet("DeleteCompany/{id}")]
        public async Task<IActionResult> DeleteCompany(int id)
        {
            try
            {
                var company = db.MCompany.Where(i => i.Id == id).FirstOrDefault();
                //by deleting the company you delete the user as well
                var email = company.Email;
                var userRecord = userManager.Users.Where(i=>i.Email==company.Email).FirstOrDefault();
                db.Remove(company);
                db.SaveChanges();
                await userManager.DeleteAsync(userRecord);
                TempData["type"] = "success";
                TempData["msg"] = "Company deleted.";
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file(source+ "DeleteCompany",ex.Message);
                TempData["type"] = "error";
                TempData["msg"] = "Error Occured";
            }
            return RedirectToAction("All");
        }


        /// <summary>
        /// return the picture from the file path
        /// </summary>
        /// <param name="image_path"></param>
        /// <returns></returns>
        [HttpGet("GetPicture")]
        public async Task<IActionResult> GetPicture(string image_path)
        {
            var filePath = System.IO.Path.Combine(Globals.uploads_folder, image_path);
            var memory = new MemoryStream();
            using (var stream = new FileStream(filePath, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
                stream.Dispose();
            }
            memory.Position = 0;//go to the beggining of the memory
            return File(memory.ToArray(), "image/jpg");
        }


        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }
    }
}
