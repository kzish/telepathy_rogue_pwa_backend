﻿using System;
using System.Collections.Generic;

namespace telepathy.Models
{
    public partial class MScannableLogos
    {
        public int Id { get; set; }
        public string ScannableLogo { get; set; }
        public string Company { get; set; }
    }
}
