﻿using System;
using System.Collections.Generic;

namespace telepathy.Models
{
    public partial class MCompany
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public sbyte? Status { get; set; }
        public string CompanyIcon { get; set; }
        public string CompanySlogan { get; set; }
    }
}
