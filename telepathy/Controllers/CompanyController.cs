﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.AspNetCore.Mvc;
using telepathy.Models;
using static System.Net.WebRequestMethods;

/// <summary>
/// handles all the interaction wiht the company
/// </summary>
namespace telepathy.Controllers
{
    [Route("Company")]
    public class CompanyController : Controller
    {
        telepathyContext db = new telepathyContext();
        private string source = "telepathy.Controllers.CompanyController.";

        /// <summary>
        /// returns a list of all the companies
        /// </summary>
        /// <returns></returns>
        [HttpGet("ListCompanies")]
        public JsonResult ListCompanies()
        {
            try
            {
                var companies = db.MCompany.ToList();
                return Json(new
                {
                    res = "ok",
                    msg = companies
                });

            }
            catch (Exception ex)
            {
                Globals.log_data_to_file(source + "ListCompanies", ex.Message);
                return Json(new
                {
                    res = "err",
                    msg = ex.Message
                });
            }
        }

        /// <summary>
        /// return the picture from the file path
        /// </summary>
        /// <param name="image_path"></param>
        /// <returns></returns>
        [HttpGet("GetPicture")]
        public async Task<IActionResult> GetPicture(string image_path) {

            //add placeholder image
            if (image_path == "null") image_path = "placeholder.png";
            if (string.IsNullOrEmpty(image_path)) image_path = "placeholder.png";

            if (!string.IsNullOrEmpty(image_path))
            {
                var filePath = System.IO.Path.Combine(Globals.uploads_folder, image_path);
                var memory = new MemoryStream();
                using (var stream = new FileStream(filePath, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                    stream.Dispose();
                }
                memory.Position = 0;
                return File(memory.ToArray(), "image/jpg");
            }
            else {
                return NotFound();
            }
        }


        /// <summary>
        /// return the video from the file path
        /// </summary>
        /// <param name="image_path"></param>
        /// <returns></returns>
        [HttpGet("GetVideo")]
        public async Task<IActionResult> GetVideo(string video_path)
        {
            if (!string.IsNullOrEmpty(video_path))
            {
                var filePath = System.IO.Path.Combine(Globals.uploads_folder, video_path);
                var memory = new MemoryStream();
                using (var stream = new FileStream(filePath, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                    stream.Dispose();
                }
                memory.Position = 0;
                return File(memory.ToArray(), "video/mp4");
            }
            else
            {
                return NotFound();
            }
        }




        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }

    }
}
