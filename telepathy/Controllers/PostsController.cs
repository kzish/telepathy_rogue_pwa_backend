﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using telepathy.Models;

/// <summary>
/// interacts with the posts
/// </summary>
namespace telepathy.Controllers
{
    [Route("Posts")]
    public class PostsController : Controller
    {
        private string source = "telepathy.Controllers.PostsController.";
        telepathyContext db = new telepathyContext();

        /// <summary>
        /// get the post from the company id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("GetPosts/{source}")]
        public IActionResult GetPosts(string source)
        {
            try
            {
                //todo: pagination
                var posts = db.MPosts.Where(i => i.Company == source).ToList();
                return Json(new
                {
                    res = "ok",
                    msg = posts
                });
            }
            catch (Exception ex)
            {
                Globals.log_data_to_file(source + "GetPosts", ex.Message);
                return Json(new
                {
                    res = "err",
                    msg = "Error ocurred"
                });
            }
        }


        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            db.Dispose();
        }


    }
}
